--payment не больше 500
insert into payment (id, type, amount, contract_id)
    values (nextval('seq_payment_id'), 'mastercard', 501, 1);

-- При заключении договора на сумму, превышающую 1000р вносить информацию по договору в таблицу аудита.
INSERT INTO contract(id, amount, details)
VALUES (101, 1500, 'Some details.');