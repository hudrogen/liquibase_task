--2. Показать самых лучших клиентов (принесших большую прибыль)

--получить коллекцию всех шипментов каждой организации, далее по каждому шипменту набор контрактов и посчитать сумму этих контрактов


SELECT id, name FROM    (select id, name, (
                SELECT sum(amount)
                FROM contract
                WHERE contract.id in (
                                    SELECT contract_id
                                    FROM shipment
                                    WHERE organization_id = organization.id
                                    )
                ) as TotalSum
    from organization
    order by TotalSum desc
    limit 1
    ) as MyNewTable




