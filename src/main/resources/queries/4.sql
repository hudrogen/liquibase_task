--4. Показать самый популярный город доставки
--локация с айди = 3 (Innopolis) должна стать самой популярной

--есть ошибка В итоге получается город, в котором находится организация с самфым большим кол-ом шипментов

SELECT city
from
location
WHERE (
    location.id =
                    (Select location_id from (
                          select *, (
                                     SELECT count(*) as count_of_orders
                                     FROM shipment
                                     WHERE shipment.organization_id = organization.id
                                    )
                          from organization
                          order by count_of_orders desc
                          limit 1) AS MyTable))

