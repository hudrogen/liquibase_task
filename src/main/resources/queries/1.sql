-- 1. Показать всех должников (чей товар доставлен, а сумма по договору не совпадает с суммой по платежам) по договорам с сортировкой по максимальному долгу
-- фактическая доставка означает не пустое значение в поле date_delivery
-- если нет пейментов, ссылающихся на договор, то по договору не произведено ни одного платежа
-- если сумма денег в пейментах меньше суммы денег по договру, то организация - должник

SELECT *
FROM organization
  WHERE id =
(
          SELECT organization_id
          FROM shipment
          WHERE organization_id = id
          AND contract_id in (
                                SELECT id  -- берем только те контракты, по которым есть задолженности.
                                FROM contract  -- таких контрактов может быть несколько
                                WHERE amount > (
                                                SELECT sum(amount)
                                                FROM payment
                                                WHERE payment.contract_id = contract.id
                                                )
                              )
          )
